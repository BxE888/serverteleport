package cz.bxe.ServerTeleport;


import net.minecraft.server.v1_11_R1.NBTTagCompound;
import net.minecraft.server.v1_11_R1.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Main extends JavaPlugin {

    private static Main instance;
    Creative crea = new Creative();
    Survival surv = new Survival();
    Minigames mini = new Minigames();
    PvP pvp = new PvP();

    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new Creative(), this);
        Bukkit.getPluginManager().registerEvents(new Minigames(), this);
        Bukkit.getPluginManager().registerEvents(new PvP(), this);
        Bukkit.getPluginManager().registerEvents(new Survival(), this);

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        getCommand("menu").setExecutor(this);

        crea.spawn();
        surv.spawn();
        mini.spawn();
        pvp.spawn();


        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                updatePlayers();
            }
        }, 20L, 20L);
    }


    public void onDisable() {
        instance = null;
        crea.remove();
        surv.remove();
        mini.remove();
        pvp.remove();
    }

    public static Main getInstance() {
        return instance;
    }

    private void updatePlayers() {
        crea.updatePlayers();
        surv.updatePlayers();
        pvp.updatePlayers();
        mini.updatePlayers();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("menu")) {
            Player p = (Player) sender;
            if (args.length == 0) {
                p.sendMessage("§cNezadal jsi nazev menu, co chceš otevřít!");
                return false;
            }
            switch (args[0].toLowerCase()) {
                case "creative":
                    crea.open(p);
                    break;
                case "minigames":
                    Minigames.open(p);
                    break;
                case "pvp":
                    PvP.open(p);
                    break;
                case "survival":
                    Survival.open(p);
                    break;
            }
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("standsreload")) {
            Player p = (Player) sender;
            if (p.hasPermission("stands.reload")) {
                crea.remove();
                surv.remove();
                mini.remove();
                pvp.remove();

                Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        crea.spawn();
                        surv.spawn();
                        mini.spawn();
                        pvp.spawn();
                    }
                }, 20L);

                p.sendMessage("§aArmorStandy načteny");
            }
        }
        return false;
    }

    public void teleportToServer(Player player, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
    }

    public void setMetadata(ArmorStand as, String paramString, Object paramObject, Main paramMain) {
        as.setMetadata(paramString, new FixedMetadataValue(paramMain, paramObject));
    }

    public static org.bukkit.inventory.ItemStack createHead(String name, String uuid, String textureData) {
        net.minecraft.server.v1_11_R1.ItemStack sHead = CraftItemStack.asNMSCopy(new org.bukkit.inventory.ItemStack(Material.SKULL_ITEM, 1, (short) 3));
        NBTTagCompound tag = new NBTTagCompound();
        NBTTagCompound skullOwnerTag = new NBTTagCompound();
        NBTTagCompound displayTag = new NBTTagCompound();
        NBTTagCompound propertiesTag = new NBTTagCompound();
        NBTTagList tagList = new NBTTagList();
        NBTTagCompound valueTag = new NBTTagCompound();
        valueTag.setString("Value", textureData);
        tagList.add(valueTag);
        propertiesTag.set("textures", tagList);
        skullOwnerTag.setString("Id", uuid);
        skullOwnerTag.setString("Name", name);
        skullOwnerTag.set("Properties", propertiesTag);
        displayTag.setString("Name", name);
        tag.set("SkullOwner", skullOwnerTag);
        tag.set("display", displayTag);
        sHead.setTag(tag);
        return CraftItemStack.asBukkitCopy(sHead);
    }

    public static ItemStack createColouredLeather(Material armourPart, int red, int green, int blue) {
        ItemStack itemStack = new ItemStack(armourPart);
        LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();
        leatherArmorMeta.setColor(Color.fromRGB(red, green, blue));
        itemStack.setItemMeta(leatherArmorMeta);
        return itemStack;
    }

}

