package cz.bxe.ServerTeleport;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;


public class Minigames implements Listener{

    private static Main plugin;
    ArmorStand as, as2, as3, as4, as5;
    Location loc = new Location(Bukkit.getWorld("Lobby"), -222.479, 70.0, 288.553, 135.5F, -3.5F);


    public static void open(Player p) {
        Inventory inv = Bukkit.createInventory(null, 36, "§e§lMinigames");

        ItemStack creative = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta creativeMeta = Bukkit.getItemFactory().getItemMeta(Material.GOLD_BLOCK);
        creativeMeta.setDisplayName("§e§lMinigames");
        ArrayList<String> creativeLore = new ArrayList<String>();
        creativeLore.add(" ");
        creativeLore.add("§7Hráči: " + ChatColor.YELLOW + getPlayers() + "/50");
        creativeLore.add("");
        creativeLore.add("§7§lPřipoj se!");
        creativeMeta.setLore(creativeLore);
        creative.setItemMeta(creativeMeta);
        inv.setItem(13, creative);
        p.openInventory(inv);
    }

    public void spawn() {
        as = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as.setGravity(false);
        as.setVisible(true);
        as.setCanPickupItems(false);
        as.setBasePlate(false);
        as.setArms(true);
        as.setCustomNameVisible(false);
        as.setHelmet(Main.getInstance().createHead("minigames", "27478235-3a09-4645-8231-63b1c5603aad", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTYxM2ZkYWI0M2Q3NjgzOGI3YjhjMTkyNDQxNjNmMTc2NWRiODc0YmRmMTUxNjk2YmRjYjY1NGViMmU1MiJ9fX0="));
        as.setChestplate(Main.getInstance().createColouredLeather(Material.LEATHER_CHESTPLATE, 255, 250, 5));
        as.setLeggings(Main.getInstance().createColouredLeather(Material.LEATHER_LEGGINGS, 255, 250, 5));
        as.setBoots(Main.getInstance().createColouredLeather(Material.LEATHER_BOOTS, 255, 250, 5));
        Main.getInstance().setMetadata((ArmorStand) as, "minigames", "minigames", Main.getInstance());

        spawnLine();
        spawnHolo();
        spawnName();
        spawnLine2();

        Bukkit.getLogger().log(Level.INFO, "§eMinigames §bArmorStand §euspesne spawnut!");
    }

    public void spawnName() {
        loc.add(0, 0.3, 0);

        as2 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as2.setGravity(false);
        as2.setCanPickupItems(false);
        as2.setBasePlate(false);
        as2.setVisible(false);
        as2.setCustomNameVisible(true);
        as2.setCustomName("§e§lMINIGAMES");
    }

    public void spawnHolo() {
        loc.add(0, 0.3, 0);
        as3 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as3.setGravity(false);
        as3.setCanPickupItems(false);
        as3.setBasePlate(false);
        as3.setVisible(false);
        as3.setCustomNameVisible(true);
        as3.setCustomName("§fPocet hracu: §e0");
    }

    public void spawnLine(){
        //loc.add(0, 0.3, 0);
        as4 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as4.setGravity(false);
        as4.setCanPickupItems(false);
        as4.setBasePlate(false);
        as4.setVisible(false);
        as4.setCustomNameVisible(true);
        as4.setCustomName("§c§f§m--------§e /\\ §f§m--------§c");
    }

    public void spawnLine2(){
        loc.add(0, 0.3, 0);
        as5 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as5.setGravity(false);
        as5.setCanPickupItems(false);
        as5.setBasePlate(false);
        as5.setVisible(false);
        as5.setCustomNameVisible(true);
        as5.setCustomName("§c§f§m--------§e \\/ §f§m--------§c");
    }

    public void remove(){
        as.remove();
        as2.remove();
        as3.remove();
        as4.remove();
        as5.remove();
    }

    public void updatePlayers() {
        as3.setCustomName("§fPocet hracu: §e" + getPlayers());
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null) {
            return;
        }
        if (e.getClickedInventory().getName() == null) {
            return;
        }
        if (!e.getClickedInventory().getName().equalsIgnoreCase("§e§lMinigames")) {
            return;
        }
        e.setCancelled(true);
        if (!e.getCurrentItem().hasItemMeta()) {
            return;
        }
        if (!e.getCurrentItem().getItemMeta().hasDisplayName()) {
            return;
        }
        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem().getType() == Material.GOLD_BLOCK) {
            Main.getInstance().teleportToServer(p, "minigames");
        }
        p.closeInventory();
    }

    public static int getPlayers() {
        try {
            Socket sock = new Socket("89.203.249.235", 25568);

            DataOutputStream out = new DataOutputStream(sock.getOutputStream());
            DataInputStream in = new DataInputStream(sock.getInputStream());

            out.write(0xFE);

            int b;
            StringBuffer str = new StringBuffer();
            while ((b = in.read()) != -1) {
                if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                    str.append((char) b);
                }
            }

            String[] data = str.toString().split("§");
            int onlinePlayers = Integer.parseInt(data[1]);
            return onlinePlayers;

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
