package cz.bxe.ServerTeleport;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;


public class Creative implements Listener {

    private static Main plugin;
    ArmorStand as, as2, as3, as4, as5;
    Location loc = new Location(Bukkit.getWorld("Lobby"), -222.484, 70.0, 270.516, 46.1F, -1.6F);


    public static void open(Player p) {
        Inventory inv = Bukkit.createInventory(null, 36, "§6§lCreative");

        ItemStack creative = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta creativeMeta = Bukkit.getItemFactory().getItemMeta(Material.GOLD_BLOCK);
        creativeMeta.setDisplayName("§6§lCreative");
        ArrayList<String> creativeLore = new ArrayList<String>();
        creativeLore.add(" ");
        creativeLore.add("§7Hráči: " + ChatColor.GOLD + getPlayers() + "/50");
        creativeLore.add("");
        creativeLore.add("§7§lPřipoj se!");
        creativeMeta.setLore(creativeLore);
        creative.setItemMeta(creativeMeta);
        inv.setItem(13, creative);
        p.openInventory(inv);
    }

    public void spawn() {
        as = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as.setGravity(false);
        as.setVisible(true);
        as.setCanPickupItems(false);
        as.setBasePlate(false);
        as.setArms(true);
        as.setCustomNameVisible(false);
        as.setHelmet(Main.getInstance().createHead("creative", "b06b1e8b-f727-4628-9e02-12331b39dd53", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjkwZDRmY2IyY2UwM2I5NGQ5MjBmMGE5ZTdhNTRiMzJjZmM3YTFkMzNhNmRmZTk3NTdkODY3OGNiYjU5MSJ9fX0="));
        as.setChestplate(Main.getInstance().createColouredLeather(Material.LEATHER_CHESTPLATE, 165, 70, 3));
        as.setLeggings(Main.getInstance().createColouredLeather(Material.LEATHER_LEGGINGS, 165, 70, 3));
        as.setBoots(Main.getInstance().createColouredLeather(Material.LEATHER_BOOTS, 165, 70, 3));
        Main.getInstance().setMetadata((ArmorStand) as, "creative", "creative", Main.getInstance());

        spawnLine();
        spawnHolo();
        spawnName();
        spawnLine2();

        Bukkit.getLogger().log(Level.INFO, "§eCreative §bArmorStand §euspesne spawnut!");
    }

    public void spawnName() {
        loc.add(0, 0.3, 0);

        as2 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as2.setGravity(false);
        as2.setCanPickupItems(false);
        as2.setBasePlate(false);
        as2.setVisible(false);
        as2.setCustomNameVisible(true);
        as2.setCustomName("§6§lCREATIVE");
    }

    public void spawnHolo() {
        loc.add(0, 0.3, 0);
        as3 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as3.setGravity(false);
        as3.setCanPickupItems(false);
        as3.setBasePlate(false);
        as3.setVisible(false);
        as3.setCustomNameVisible(true);
        as3.setCustomName("§fPocet hracu: §60");
    }

    public void spawnLine(){
        //loc.add(0, 0.3, 0);
        as4 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as4.setGravity(false);
        as4.setCanPickupItems(false);
        as4.setBasePlate(false);
        as4.setVisible(false);
        as4.setCustomNameVisible(true);
        as4.setCustomName("§c§f§m--------§6 /\\ §f§m--------§c");
    }

    public void spawnLine2(){
        loc.add(0, 0.3, 0);
        as5 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as5.setGravity(false);
        as5.setCanPickupItems(false);
        as5.setBasePlate(false);
        as5.setVisible(false);
        as5.setCustomNameVisible(true);
        as5.setCustomName("§c§f§m--------§6 \\/ §f§m--------§c");
    }

    public void remove(){
        as.remove();
        as2.remove();
        as3.remove();
        as4.remove();
        as5.remove();
    }

    public void updatePlayers() {
        as3.setCustomName("§fPocet hracu: §6" + getPlayers());
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null) {
            return;
        }
        if (e.getClickedInventory().getName() == null) {
            return;
        }
        if (!e.getClickedInventory().getName().equalsIgnoreCase("§6§lCreative")) {
            return;
        }
        e.setCancelled(true);
        if (!e.getCurrentItem().hasItemMeta()) {
            return;
        }
        if (!e.getCurrentItem().getItemMeta().hasDisplayName()) {
            return;
        }
        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem().getType() == Material.GOLD_BLOCK) {
            Main.getInstance().teleportToServer(p, "creative");
        }
        p.closeInventory();
    }

    public static int getPlayers() {
        try {
            Socket sock = new Socket("89.203.249.235", 25570);

            DataOutputStream out = new DataOutputStream(sock.getOutputStream());
            DataInputStream in = new DataInputStream(sock.getInputStream());

            out.write(0xFE);

            int b;
            StringBuffer str = new StringBuffer();
            while ((b = in.read()) != -1) {
                if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                    str.append((char) b);
                }
            }

            String[] data = str.toString().split("§");
            int onlinePlayers = Integer.parseInt(data[1]);
            return onlinePlayers;

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
